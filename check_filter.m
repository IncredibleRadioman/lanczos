%%
clear
clc
close all
%%
table = zeros(7,5);
itable = 1;
delta = 0.34;
for dr=60:10:120
    
    %������ ����������� �������� �������� �������
    fs = 250e6;
    T = 2^17/fs;
    for signal_bitness=8:64
        
        t = 0:1/fs:2^17/fs;
        f = 10e6;
        
        s = 0.9*sin(2*pi*f*t);
        
        s_t = round(s * 2^(signal_bitness-1));
        
        result = sfdr(s_t,fs);
        
        if(result>=dr)
            break;
        end
        
        
    end
    
    SB = signal_bitness;
    %����� ��������� ����������� ������� ������� �������
    for a=1:200
        
        [x_out, fl_data] = resampling_lanczos(s_t,2^17,delta+1,a);
        result = sfdr(fl_data,fs);
        if(result>=dr)
            break;
        end
    end
    
    min_a = a;
    
    for delta_bitness=8:24
        L_bitness=24;
        signal_bitness = SB;
        a = min_a;
        ROMs = getROMs (a,delta_bitness,L_bitness);
        FileName = strcat('lanczos_v5_sfdr_',num2str(dr),'_db_',num2str(delta_bitness),'_Lb_',num2str(L_bitness));
        T = 2^17/fs;
        createModel;
        %%
        t = 0:1/fs:2^17/fs;
        f = 10e6;
        
        s = 0.9*sin(2*pi*f*t);
        c = 0.9*cos(2*pi*f*t);
        
        s_t = round(s * 2^(signal_bitness-1));
        c_t = round(c * 2^(signal_bitness-1));
        
        s_fi= ficonv(s_t,signal_bitness);
        c_fi = ficonv(c_t,signal_bitness);
        
        buf_data = s_fi + (2^signal_bitness)*c_fi;
        %%
        inp_signal.time = [];
        inp_signal.signals.values=buf_data.';
        inp_signal.signals.dimensions=1;
        %%
        [x_out, fl_data] = resampling_lanczos(s_t,2^17,delta+1,a);
        out = sim(FileName);
        save_system;
        close_system;
        delete(strcat(FileName,'.slx'));
        delete(strcat(FileName,'.slxc'));
        delete(strcat(FileName,'_sysgen.log'));
        %%
        clear buf_data;
        
        buf_data = out.lanczos_outp_tdata;
        buf_valid = out.lanczos_outp_tvalid;
        
        
        
        data = [];
        
        for j=1:length(buf_data)
            
            
            if(buf_valid(j) ~= 0)
                
                
                data = [data buf_data(j)];
                
                
            end
            
            
        end
        
        if (length(fl_data) < length(data))
            l = length(fl_data);
        else
            l = length(data);
        end
        %%
        dif = data(1:l) - fl_data(1:l);
        if(max(dif)<0.5)
            break;
        end
        
    end
    db_min = delta_bitness;
    Lb_min = L_bitness;
    
    table(itable,1) = dr;
    table(itable,2) = SB;
    table(itable,3) = min_a;
    table(itable,4) = delta_bitness;
    table(itable,5) = Lb_min;
    
    
    
    itable = itable + 1;
    
end

table