function y = getROMs (a,bitness,out_bitness)
% a  - �������� ������� �������
% bitness - �������� delta
% out_bitness - �������� ��������� �������

y = zeros(2*a,2^bitness);

for i=1:2*a
    for j=0:2^bitness
        
        frac_part = j / 2^bitness;
        x = (i - (a+1)) + frac_part;
        
        y(i,j+1) = sinc(x) * sinc(x/a);
    end
end