%%
%��� ������ ��������� ���������
for line=length(summs):-1:2
    
    sum_last = summs(line); %������ ����� ���������� � ��������  � ������� �����
    delay_last = delays(line);
    
    %������ � ����������
    sum_prev = summs(line-1);
    delay_prev = delays(line-1);
    
    %����� ����� ��������� � ������
    elements_last = sum_last + delay_last;
    elements_prev = sum_prev + delay_prev;
    
    %������ ���������� ����� (�������� 3 0 0 1 - ����� 3, ������� 0, ����
    %0, ��������� ����� - ��� ��������: 1 - ��������, 0 - ��)
    %�����
    in_ports = [];
    for element=1:elements_last
        
        if(element<=sum_last)
            
            port1 = line*100000 + (element-1)*100 + 1*10 + 2;
            port2 = line*100000 + (element-1)*100 + 2*10 + 2;
            in_ports = [in_ports port1 port2];
            
        else
            port = line*100000 + (element-1)*100+1*10 + 1;
            in_ports = [in_ports port];
        end
        
    end
    out_ports = [];
    for element=1:elements_prev
        if(element<=sum_prev)
            
            port = (line-1)*100000 + (element-1)*100 + 1*10 + 2;
            out_ports = [out_ports port];
            
        else
            port = (line-1)*100000 + (element-1)*100+1*10 + 1;
            out_ports = [out_ports port];
        end
    end
    %����� ��������, ������ ����� �� ���������
    index = 1; %����� �������� ��������� �����
    for i=1:length(in_ports)
        
        %��������� ������� ����
        nowport = in_ports(i);
        lin = floor(nowport/100000);
        nowport = nowport - 100000*lin;
        element = floor(nowport/100);
        nowport = nowport - 100*element;
        port = floor(nowport/10);
        whatisit = nowport - 10*port;
        if (whatisit > 1)
            
            in = strcat('Line',num2str(lin),'Sum',num2str(element),'/',num2str(port));
            
        else
            
            in = strcat('SumDelay',num2str(lin),'/',num2str(port));
            
        end
        %�������� ������� ����, ������ ��������
        nowport = out_ports(index);
        lin = floor(nowport/100000);
        nowport = nowport - 100000*lin;
        element = floor(nowport/100);
        nowport = nowport - 100*element;
        port = floor(nowport/10);
        whatisit = nowport - 10*port;
        
        if(whatisit>1)
            
            out = strcat('Line',num2str(lin),'Sum',num2str(element),'/',num2str(port));
            
        else
            
            out = strcat('SumDelay',num2str(lin),'/',num2str(port));
            
        end
        
        
        add_line(SSP,out,in,'autorouting','on');
        index = index + 1;
        
    end
    
end
%%
%����� ������� ������, ������ ����� ��������� ���������� � �����������
%����������� ������ 2*a, � ��������� �� ���� ���������� - a
for i=0:(a-1)

    in1 = strcat('Line1Sum',num2str(i),'/1');
    in2 = strcat('Line1Sum',num2str(i),'/2');
    
    out1 = strcat('Mult',num2str(2*i),'/1');
    out2 = strcat('Mult',num2str(2*i+1),'/1');
    
    add_line(SSP,out1,in1,'autorouting','on');
    add_line(SSP,out2,in2,'autorouting','on');
    
end

%%
%��������� ���������� � ������� ���������� (��� �����)
for i=0:(2*a-1)
    
    in1 = strcat('Mult',num2str(i),'/1');
    in2 = strcat('Mult',num2str(i),'/2');
    
    out1 = strcat('ROMDelay',num2str(i),'/1');
    out2 = strcat('ROM',num2str(i),'/1');
    
    add_line(SSP,out1,in1,'autorouting','on');
    add_line(SSP,out2,in2,'autorouting','on');
    
end

%%
%���� �� �����
%��������� ROMdelay � MultDelay
for i=0:(2*a-1)

    in1 = strcat('ROMDelay',num2str(i),'/1');
    out1 = strcat('MultDelay',num2str(i),'/1');
    add_line(SSP,out1,in1,'autorouting','on');
    
end
%%
%multdelay � Delay
for i=1:(2*a-1)

    in1 = strcat('MultDelay',num2str(i),'/1');
    out1 = strcat('Delay',num2str(i),'/1');
    add_line(SSP,out1,in1,'autorouting','on');
    
end

%%
%delay ����� �����
for i=2:(2*a-1)
    
    in1 = strcat('Delay',num2str(i),'/1');
    out1 = strcat('Delay',num2str(i-1),'/1');
    add_line(SSP,out1,in1,'autorouting','on');
end
%%
%���� inp_data ����� ��������� � multdelay0 � delay1
in1 = strcat('MultDelay0/1');
in2 = strcat('Delay1/1');

out1 = strcat('inp_data/1');

add_line(SSP,out1,in1,'autorouting','on');
add_line(SSP,out1,in2,'autorouting','on');
%%
%��������� ROM� � CMult
out1 = strcat('Convert/1');
for i=0:(2*a-1)

    in1 = strcat('ROM',num2str(i),'/1');
    add_line(SSP,out1,in1,'autorouting','on');
    
end
%%
add_line(SSP,'Reinterpret/1','Convert/1','autorouting','on');
%%
%��������� delta � CMult���
add_line(SSP,'delta/1','Reinterpret/1','autorouting','on');
%%
%��������� ����������
add_line(SSP,'inp_valid/1','ValidDelay/1','autorouting','on');
add_line(SSP,'ValidDelay/1','valid/1','autorouting','on');
add_line(SSP,strcat('Line',num2str(length(summs)),'Sum0/1'),'data/1','autorouting','on');

%%
%���������� ���������, ������ ����� ������� � ������� ������
add_line(FileName,'From Workspace/1','inp_tdata/1','autorouting','on');
add_line(FileName,'inp_tdata/1','IQToBus/1','autorouting','on');
add_line(FileName,'IQToBus/1','SignalDelay/1','autorouting','on');
add_block('simulink/Sinks/Terminator',strcat(FileName,'/Terminator'));
add_line(FileName,'IQToBus/2','Terminator/1','autorouting','on');
add_line(FileName,'SignalDelay/1','Lanczos/1','autorouting','on');


add_line(FileName,'Constant1/1','ConstDelay/1','autorouting','on');
add_line(FileName,'ConstDelay/1','AND/1','autorouting','on');
add_line(FileName,'AND/1','ValidDelay/1','autorouting','on');
add_line(FileName,'ValidDelay/1','Lanczos/2','autorouting','on');

add_line(FileName,'Constant/1','inp_delta/1','autorouting','on');
add_line(FileName,'inp_delta/1','delta/1','autorouting','on');
add_line(FileName,'delta/1','AND/2','autorouting','on');
add_line(FileName,'delta/2','DeltaDelay/1','autorouting','on');
add_line(FileName,'DeltaDelay/1','Lanczos/3','autorouting','on');
%%
%���� �� �����
add_line(FileName,'Lanczos/1','outp_tdata/1','autorouting','on');
add_line(FileName,'Lanczos/2','outp_tvalid/1','autorouting','on');
add_line(FileName,'outp_tdata/1','dataToDbl/1','autorouting','on');
add_line(FileName,'outp_tvalid/1','validToDbl/1','autorouting','on');
add_line(FileName,'dataToDbl/1','dat/1','autorouting','on');
add_line(FileName,'validToDbl/1','val/1','autorouting','on');