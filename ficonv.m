function [output] = ficonv(input,bitness)


    for i=1:length(input)
    
        if(input(i)<0)
            
           output(i) = 2^bitness + input(i);
            
        else
        
            output(i) = input(i);
            
        end
    
    end


end