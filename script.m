%%
clear
clc
close all
%%
fs = 250e6;
a = 6;
delta = 0.34;

delta_bitness = 12; %разрядность delta
L_bitness = 24; %разрядность L

signal_bitness = 9;
T = 2^17/fs;
%%
ROMs = getROMs (a,delta_bitness,L_bitness);
isNew = 1;
FileName = strcat('lanczos_v5_a',num2str(a));
if (isNew > 0)
    createModel;
end
%%

t = 0:1/fs:2^17/fs;
f = 10e6;

s = 0.9*sin(2*pi*f*t);
c = 0.9*cos(2*pi*f*t);

s_t = round(s * 2^(signal_bitness-1));
c_t = round(c * 2^(signal_bitness-1));

s_fi= ficonv(s_t,signal_bitness);
c_fi = ficonv(c_t,signal_bitness);

buf_data = s_fi + (2^signal_bitness)*c_fi;
%%
inp_signal.time = [];
inp_signal.signals.values=buf_data.';
inp_signal.signals.dimensions=1;

%%

[x_out, fl_data] = resampling_lanczos(s_t,2^17,delta+1,a);
%%
out = sim(FileName);
%%
clear buf_data;

buf_data = out.lanczos_outp_tdata;
buf_valid = out.lanczos_outp_tvalid;



data = [];

for i=1:length(buf_data)
   
    
    if(buf_valid(i) ~= 0)
       
        
        data = [data buf_data(i)];
        
        
    end
    
    
end

if (length(fl_data) < length(data))
    l = length(fl_data);
else
    l = length(data);
end

dif = data(1:l) - fl_data(1:l);
figure
plot(dif);
grid on
title('Difference between floating and fixed point');


%%
save_system(FileName);
close_system(FileName);
%%
%исходный сигнал + fxpoint + flpoint
spect_fl = fftshift(abs(fft(fl_data,2^16)));
spect_fx = fftshift(abs(fft(data,2^16)));
spect = fftshift(abs(fft(s_t,2^16)));

step = fs/length(spect);
freq = -fs/2:step:fs/2-step;

figure;
title('Specters');
plot(freq,10*log10(spect),'r');
hold on
plot(freq,10*log10(spect_fl),'g');
hold on
plot(freq,10*log10(spect_fx),'b');
grid on;
xlim([0, 10*f]);
legend('Default signal','Lanczos (floating point)','Lanczos (fixed point)');

%%
figure;
period = 1/f;
period = 5*period;
finish = floor(period*fs);
title('Signals');
plot(t(1:finish),s_t(1:finish),'Color','black');
hold on
plot(t(1:finish),data(1:finish),'Color','red','LineStyle',':');
hold on
plot(t(1:finish),fl_data(1:finish),'Color','blue','LineStyle','--');
grid on
legend('Default signal','Lanczos (floating point)','Lanczos (fixed point)');
%%
figure;sfdr(data,fs); figure; sfdr(fl_data,fs);figure; sfdr(s_t,fs);