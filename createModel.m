%%
%�� ����� a - ������� �������
%fs - ������� ������������
%bitness - ����������� ROM (����� ���, ���������� �� delta)
%out_bitness - ����������� ����� ROM (����� ��� �� L(x))
%ROMs - ������ ������ ��� L(x)
%T - ����� �������������

new_system(FileName);
open_system(FileName);
save_system(FileName);

%%   System & Solver Config
set_param(FileName,'Solver','FixedStep');
set_param(FileName,'FixedStep',num2str(1/fs));
set_param(FileName,'StopTime',num2str(T));

%%
%������� sysgen
add_block('xbsBasic_r4/ System Generator',strcat(FileName,'/ System Generator'));
%����������� ��� ��������� (�� ����� ���� ������ 1 - ������ � ���������)
set_param(strcat(FileName,'/ System Generator'), 'simulink_period',num2str(1/fs));
%%
%������� ���� �������� �������
add_block('simulink/Sources/From Workspace',strcat(FileName,'/From Workspace'));
%%
set_param(strcat(FileName,'/From Workspace'),'VariableName','inp_signal');
set_param(strcat(FileName,'/From Workspace'),'SampleTime','-1');
set_param(strcat(FileName,'/From Workspace'),'OutDataTypeStr','double');
set_param(strcat(FileName,'/From Workspace'),'Interpolate','off');
set_param(strcat(FileName,'/From Workspace'),'OutputAfterFinalValue','Cyclic repetition');
set_param(strcat(FileName,'/From Workspace'),'position',[100 100 200 150]);

%%
%���������� ������� ���� delta
add_block('simulink/Sources/Constant',strcat(FileName,'/Constant'));
%%
set_param(strcat(FileName,'/Constant'),'position',[100 400 200 450]);
set_param(strcat(FileName,'/Constant'),'Value','delta');

%%
%������� ������� ����� Sysgen
add_block('xbsBasic_r4/Gateway In',strcat(FileName,'/inp_tdata'));
%%
set_param(strcat(FileName,'/inp_tdata'), 'position',[250 115 300 135]);
set_param(strcat(FileName,'/inp_tdata'), 'arith_type','Unsigned');
set_param(strcat(FileName,'/inp_tdata'), 'n_bits','2*signal_bitness');
set_param(strcat(FileName,'/inp_tdata'), 'bin_pt','0');
set_param(strcat(FileName,'/inp_tdata'), 'quantization','Round  (unbiased: +/- Inf)');
set_param(strcat(FileName,'/inp_tdata'), 'period',num2str(1/fs));


%%
add_block('xbsBasic_r4/Gateway In',strcat(FileName,'/inp_delta'));
%%
set_param(strcat(FileName,'/inp_delta'), 'position',[250 415 300 435]);
set_param(strcat(FileName,'/inp_delta'), 'arith_type','Unsigned');
set_param(strcat(FileName,'/inp_delta'), 'n_bits','64');
set_param(strcat(FileName,'/inp_delta'), 'bin_pt','64');
set_param(strcat(FileName,'/inp_delta'), 'quantization','Round  (unbiased: +/- Inf)');
set_param(strcat(FileName,'/inp_delta'), 'period',num2str(1/fs));

%%
%������ �������� �� lanczos_v2 ����� IQToBus � Subsystem
add_block('proto/IQToBus', strcat(FileName,'/IQToBus'));
add_block('proto/delta', strcat(FileName,'/delta'));
%%
set_param(strcat(FileName,'/IQToBus'), 'position',[350 75 450 175]);
begins = strcat('a={b[',num2str(signal_bitness-1),':0]}');
ends = strcat('c={b[',num2str(2*signal_bitness-1),':',num2str(signal_bitness),']}');
expr = [begins native2unicode(10) ends];

set_param(strcat(FileName,'/IQToBus/BitBasher'), 'bitexpr',expr);

clear begins;
clear ends;
clear expr;

set_param(strcat(FileName,'/delta'), 'position',[350 375 450 475]);

%%
%����� ������� - ���� ���������� �������� �������
add_block('proto/Lanczos', strcat(FileName,'/Lanczos'));
%%
%��������� �������������
set_param(strcat(FileName,'/Lanczos'), 'position',[800 175 950 375]);
SSP = strcat(FileName,'/Lanczos');
set_param(strcat(SSP,'/inp_data'), 'position',[50 50 100 75]);
%%
%��������� �������� ����� ��������
z_w = 50;
z_h = 50;
start_x = 200;
start_y = 150;
dy = 20;
%������� �� �������� ������� (������������� ��������)
for i=1:(2*a-1)
   
    DelayName = strcat('Delay',num2str(i));
    add_block('xbsBasic_r4/Delay',strcat(SSP,'/',DelayName));
    set_param(strcat(SSP,'/',DelayName), 'position',[start_x-z_w/2, i*start_y-z_h/2, start_x+z_w/2, i*start_y + z_h/2]);
    set_param(strcat(SSP,'/',DelayName),'Orientation','down');
end

%������� ��-�������������� ��� �����������
%������� �������� ������
start_x = 400;
DelayName = 'MultDelay0';
add_block('xbsBasic_r4/Delay',strcat(SSP,'/',DelayName));
set_param(strcat(SSP,'/',DelayName), 'position',[start_x-z_w/2, 63-z_h/2, start_x+z_w/2, 63 + z_h/2]);
set_param(strcat(SSP,'/',DelayName),'latency','3');
start_y = 225;
step = 150;
for i=1:(2*a-1)
   
    DelayName = strcat('MultDelay',num2str(i));
    add_block('xbsBasic_r4/Delay',strcat(SSP,'/',DelayName));
    set_param(strcat(SSP,'/',DelayName), 'position',[start_x-z_w/2, (start_y + (i-1)*step)-z_h/2, start_x+z_w/2, (start_y + (i-1)*step) + z_h/2]);
    set_param(strcat(SSP,'/',DelayName),'latency','3');
end

%������� ��-�������������� ��� ROM-������
%������� �������� ������
start_x = 600;
DelayName = 'ROMDelay0';
add_block('xbsBasic_r4/Delay',strcat(SSP,'/',DelayName));
set_param(strcat(SSP,'/',DelayName), 'position',[start_x-z_w/2, 63-z_h/2, start_x+z_w/2, 63 + z_h/2]);
set_param(strcat(SSP,'/',DelayName),'latency','1');
start_y = 225;
step = 150;
for i=1:(2*a-1)
   
    DelayName = strcat('ROMDelay',num2str(i));
    add_block('xbsBasic_r4/Delay',strcat(SSP,'/',DelayName));
    set_param(strcat(SSP,'/',DelayName), 'position',[start_x-z_w/2, (start_y + (i-1)*step)-z_h/2, start_x+z_w/2, (start_y + (i-1)*step) + z_h/2]);
    set_param(strcat(SSP,'/',DelayName),'latency','1');
end

%%
%������ �������� � delta
set_param(strcat(SSP,'/delta'), 'position',[50, (2*a+1)*step-12.5, 100, (2*a+1)*step+12.5]);
delta_y = (2*a+1)*step;
%%
%������� ��������������� delta � ����� ������
add_block('xbsTypes_r4/Reinterpret',strcat(SSP,'/','Reinterpret'));
%%
set_param(strcat(SSP,'/Reinterpret'), 'position',[150,(delta_y - 50),250,(delta_y + 50)]);
set_param(strcat(SSP,'/Reinterpret'), 'force_arith_type','on');
set_param(strcat(SSP,'/Reinterpret'), 'force_bin_pt','on');
set_param(strcat(SSP,'/Reinterpret'), 'bin_pt','64-delta_bitness');
%%
add_block('xbsBasic_r4/Convert',strcat(SSP,'/','Convert'));
%%
set_param(strcat(SSP,'/Convert'), 'position',[350,(delta_y  - 50),450,(delta_y+ 50)]);
set_param(strcat(SSP,'/Convert'), 'arith_type','Unsigned');
set_param(strcat(SSP,'/Convert'), 'bin_pt','0');
set_param(strcat(SSP,'/Convert'), 'n_bits','delta_bitness');
set_param(strcat(SSP,'/Convert'), 'latency','3');
set_param(strcat(SSP,'/Convert'), 'quantization','Round  (unbiased: +/- Inf)');
set_param(strcat(SSP,'/Convert'), 'overflow','Saturate');
% for i=0:(2*a-1)
% 
%     MultName = strcat('CMult',num2str(i));
%     add_block('xbsMath_r4/CMult',strcat(SSP,'/',MultName));
%     set_param(strcat(SSP,'/',MultName), 'position',[350,(delta_y +i*step - 50),450,(delta_y +i*step + 50)]);
%     set_param(strcat(SSP,'/',MultName),'latency','3');
%     set_param(strcat(SSP,'/',MultName),'const','2^delta_bitness');
%     set_param(strcat(SSP,'/',MultName),'const_n_bits','delta_bitness');
%     set_param(strcat(SSP,'/',MultName),'const_bin_pt','0');
%     set_param(strcat(SSP,'/',MultName),'precision','User defined');
%     set_param(strcat(SSP,'/',MultName),'arith_type','Unsigned');
%     set_param(strcat(SSP,'/',MultName),'n_bits','delta_bitness');
%     set_param(strcat(SSP,'/',MultName),'bin_pt','0');
%     set_param(strcat(SSP,'/',MultName),'quantization','Round  (unbiased: +/- Inf)');
%     set_param(strcat(SSP,'/',MultName),'overflow','Saturate');
%     
% end
%%
%������� ����� ROM-������
for i=0:(2*a-1)

    MemName = strcat('ROM',num2str(i));
    add_block('xbsMemory_r4/ROM',strcat(SSP,'/',MemName));
    set_param(strcat(SSP,'/',MemName), 'position',[550,(delta_y +i*step - 50),650,(delta_y +i*step + 50)]);
    set_param(strcat(SSP,'/',MemName),'latency','1');
    set_param(strcat(SSP,'/',MemName),'depth','2^delta_bitness');
    set_param(strcat(SSP,'/',MemName),'initVector',strcat('ROMs(',num2str(i+1),',:)'));
    set_param(strcat(SSP,'/',MemName),'bin_pt','0');
    set_param(strcat(SSP,'/',MemName),'n_bits','L_bitness+1');
    set_param(strcat(SSP,'/',MemName),'bin_pt','L_bitness');
end

%%
%��������� ����� �������� ��������������
delta_y = a * step;
for i=0:(2*a-1)

    MultName = strcat('Mult',num2str(i));
    add_block('xbsMath_r4/Mult',strcat(SSP,'/',MultName));
    set_param(strcat(SSP,'/',MultName), 'position',[950,(delta_y +i*step - 50),1050,(delta_y +i*step + 50)]);
    set_param(strcat(SSP,'/',MultName),'latency','3');
    set_param(strcat(SSP,'/',MultName),'precision','User defined');
    set_param(strcat(SSP,'/',MultName),'n_bits','L_bitness + signal_bitness');
    set_param(strcat(SSP,'/',MultName),'bin_pt','L_bitness');
    set_param(strcat(SSP,'/',MultName),'quantization','Round  (unbiased: +/- Inf)');
    set_param(strcat(SSP,'/',MultName),'overflow','Saturate');
    
end

%%
%���������
c = 2*a;
i = 1;
while(c>1)
    summs(i) = floor(c/2); %���������� ����� ���������� � ������ �� �����
    delays(i) = mod(c,2); %���������� ��������
    c = summs(i) + delays(i);
    i = i+1;
end

delta_x = 1200;

for line=1:length(summs)
    sum = summs(line);
    for i=0:sum-1
        
        SumName = strcat('Line',num2str(line),'Sum',num2str(i));
        add_block('xbsMath_r4/AddSub',strcat(SSP,'/',SumName));
        set_param(strcat(SSP,'/',SumName), 'position',[delta_x + (line-1)*200 - 50,(delta_y +i*step - 50),delta_x + (line-1)*200 + 50,(delta_y +i*step + 50)]);
        set_param(strcat(SSP,'/',SumName),'latency','1');
        set_param(strcat(SSP,'/',SumName),'precision','User defined');
        set_param(strcat(SSP,'/',SumName),'arith_type',"Signed  (2's comp)");
        if(line~=length(summs))
            set_param(strcat(SSP,'/',SumName),'n_bits',strcat('L_bitness+signal_bitness+',num2str(line)));
            set_param(strcat(SSP,'/',SumName),'bin_pt','L_bitness');
        else
            set_param(strcat(SSP,'/',SumName),'bin_pt','0');
            set_param(strcat(SSP,'/',SumName),'n_bits',strcat('signal_bitness'));
        end
        set_param(strcat(SSP,'/',SumName),'quantization','Round  (unbiased: +/- Inf)');
        set_param(strcat(SSP,'/',SumName),'overflow','Saturate');
    end
    q = delays(line);
    if(q>0)
       
        add_block('xbsBasic_r4/Delay',strcat(SSP,'/','SumDelay',num2str(line)));
        set_param(strcat(SSP,'/','SumDelay',num2str(line)), 'position',[delta_x + (line-1)*200 - 50,(delta_y +sum*step - 50),delta_x + (line-1)*200 + 50,(delta_y +sum*step + 50)]);
        set_param(strcat(SSP,'/','SumDelay',num2str(line)),'latency','1');
    end
    
    
end
%%
%������������ valid
delay_valid = 3+1+3+length(summs);
delta_y = (4*a+2)*step;
set_param(strcat(SSP,'/inp_valid'), 'position',[50, delta_y-12.5, 100, delta_y+12.5]);
add_block('xbsBasic_r4/Delay',strcat(SSP,'/','ValidDelay'));
set_param(strcat(SSP,'/','ValidDelay'), 'position',[175,delta_y - 25,225,delta_y + 25]);
set_param(strcat(SSP,'/','ValidDelay'),'latency',num2str(delay_valid));


%%
%��������� ������ ����������
delta_x = 1200 + (length(summs)+15)*200;
set_param(strcat(SSP,'/data'), 'position',[delta_x, 50, delta_x+50, 75]);
set_param(strcat(SSP,'/valid'), 'position',[delta_x, 350, delta_x+50, 375]);

%%
%���������� ������, ������ ����� ������
%�������� �������� �������
add_block('xbsBasic_r4/Delay',strcat(FileName,'/SignalDelay'));
set_param(strcat(FileName,'/SignalDelay'), 'position',[600,100-12,650,100+12]);
set_param(strcat(FileName,'/','SignalDelay'),'latency','1');
%%
%��������� ��� ������� ������� ���������� ������� ��������
add_block('xbsBasic_r4/Constant',strcat(FileName,'/Constant1'));
set_param(strcat(FileName,'/Constant1'), 'position',[100,250,150,300]);
set_param(strcat(FileName,'/Constant1'), 'arith_type','Boolean');
set_param(strcat(FileName,'/Constant1'), 'explicit_period',1);
set_param(strcat(FileName,'/Constant1'), 'period',num2str(1/fs));
%%
%������� �������� ��� ���
add_block('xbsBasic_r4/Delay',strcat(FileName,'/ConstDelay'));
set_param(strcat(FileName,'/ConstDelay'), 'position',[250,250,300,300]);
set_param(strcat(FileName,'/','ConstDelay'),'latency',num2str(a-1));

%%
%������� ���������� �
add_block('xbsControl_r4/Logical',strcat(FileName,'/AND'));
set_param(strcat(FileName,'/AND'), 'position',[400,250,450,300]);
set_param(strcat(FileName,'/','AND'),'latency','1');
%%
%������� �� ��� valid � delta
add_block('xbsBasic_r4/Delay',strcat(FileName,'/ValidDelay'));
set_param(strcat(FileName,'/ValidDelay'), 'position',[500,250,550,300]);
set_param(strcat(FileName,'/','ValidDelay'),'latency',num2str(a+1));

add_block('xbsBasic_r4/Delay',strcat(FileName,'/DeltaDelay'));
set_param(strcat(FileName,'/DeltaDelay'), 'position',[500,350,550,400]);
set_param(strcat(FileName,'/','DeltaDelay'),'latency',num2str(a+2));

%%
%�������� �����
add_block('proto/outp_tdata',strcat(FileName,'/outp_tdata'));
set_param(strcat(FileName,'/outp_tdata'), 'position',[1100,115,1150,135]);
add_block('proto/outp_tdata',strcat(FileName,'/outp_tvalid'));
set_param(strcat(FileName,'/outp_tvalid'), 'position',[1100,215,1150,235]);

%�������������� ����� ������
add_block('proto/Data Type Conversion',strcat(FileName,'/dataToDbl'));
set_param(strcat(FileName,'/dataToDbl'), 'position',[1200,115,1300,135]);

add_block('proto/Data Type Conversion',strcat(FileName,'/validToDbl'));
set_param(strcat(FileName,'/validToDbl'), 'position',[1200,215,1300,235]);

%� workSpace
add_block('proto/To Workspace',strcat(FileName,'/dat'));
set_param(strcat(FileName,'/dat'), 'position',[1350,115,1450,135]);

add_block('proto/To Workspace1',strcat(FileName,'/val'));
set_param(strcat(FileName,'/val'), 'position',[1350,215,1450,235]);

%%
%�������� ���������� ����������
createLines;

%%
%��� ������, ������ ������� workspace �� �������
clear SSP;
clear delay_last;
clear delay_prev;
clear c;
clear delay_valid;
clear DelayName;
clear delays;
clear delta_x;
clear delta_y;
clear dy;
clear element;
clear elements_last;
clear elements_prev;
clear i;
clear in;
clear in1;
clear in2;
clear in_ports;
clear index;
clear lin;
clear line;
clear MemName;
clear MultName;
clear nowport;
clear out1;
clear out2;
clear out_ports;
clear port;
clear port1;
clear port2;
clear q;
clear start_x;
clear start_y;
clear step;
clear out;
clear sum;
clear sum_last;
clear sum_prev;
clear summs;
clear SumName;
clear whatisit;
clear z_h;
clear z_w;

%%
save_system(FileName);
close_system(FileName);