function [x_out,y] =resampling_lanczos(x,n,r,a)
% Inputs:
%   x     - input samples
%   n     - number of input samples
%               n>1 -> decimation
%               n<1 -> interpolation
%   r     - rate change factor
%   a     - Lanczos kernel size
% Ouputs:
%   x_out - output samples positions
%   y     - output samples
m = floor((n-1)/r);
y = zeros(1,m);
x_out = zeros(1,m);
% m - number of output samples
% y = zeros(1,m);
% % % % % % % %
k=1;
for j = 1:m
    start_index = floor(j*r)-a+1;
    finish_index = floor(j*r)+a;
    if (floor(j*r)-a+1>=0)&&(floor(j*r)+a<(n-1))
        for i = floor(j*r)-a+1 : floor(j*r)+a
            y(k)= y(k)+x(i+1)*lanczos_kernel(j*r - i,a);
            
        end
        x_out(k)=j*r +1;
        k=k+1;
    end
end
y = y(1:k-1);
x_out = x_out(1:k-1);

function [L]=lanczos_kernel(x,a)
if(abs(x)<a)
    if (x == 0)
        L = 1;
    else
      L = sin(pi*x)*sin(pi*x/a)/(pi*pi*x*x)*a;
    end
else
    L = 0;
end